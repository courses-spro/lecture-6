import React, { useState } from 'react';
import styles from '../../styles/Users.module.css';

export function AddUser(props) {
  const [userName, setName] = useState('');
  const [userAge, setAge] = useState('');


  return <div className={styles.userItem}>
    <div>
      <input type={'text'} name={'username'} onChange={(ev) => setName(ev.target.value)} value={userName}/>
      <input type={'number'} name={'userAge'} onChange={(ev) => setAge(ev.target.value)} value={userAge}/>
    </div>
    <button className={styles.createButton} onClick={() => props.onCreate({ name: userName, age: userAge })}>Create
    </button>

  </div>;

}
