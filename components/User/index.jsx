import React from 'react';
import PropTypes from 'prop-types';
import styles from  '../../styles/Users.module.css';

function UserItem(props) {
  const user = props.user;

  return <div className={styles.userItem}>
    <p>{user.name} {user.age}</p>
    <button className={styles.delButton} onClick={() => props.onDelete(user.id)}>Delete</button>
  </div>
}

UserItem.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserItem;
