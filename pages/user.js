import UserItem from '../components/User';
import { useEffect, useState } from 'react';
import styles from '../styles/Users.module.css';
import { serverUrl } from '../constants';
import { AddUser } from '../components/AddUser';

function UsersPage(props) {
  const [users, setUsers] = useState([]);

  async function fetchUsers() {
    console.log('fetching users')
    const users = await (await fetch(`${serverUrl}/user`)).json();
    console.log('users', users);
    setUsers( users);
  }

  useEffect(() => {
    if (!users.length) {
      fetchUsers()
    }
  });

  async function handleAdd(user) {
    await fetch(`${serverUrl}/user`, { method: 'POST', body: JSON.stringify(user), headers: { Accept: 'application/json' } });
    await fetchUsers();
  }

  async function handleDelete(userId) {
    await fetch(`${serverUrl}/user/${userId}`, { method: 'DELETE' });
    await fetchUsers();
  }


  return <div className={styles.userComponent}>
    {users.map(user =>
      <UserItem key={user.id} user={user} onDelete={handleDelete} />
    )}
    <AddUser onCreate={handleAdd} />
  </div>
}

export default UsersPage;
