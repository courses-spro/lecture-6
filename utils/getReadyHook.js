import { useEffect, useState } from 'react';
import { serverUrl } from '../constants';

const RE_TIMEOUT = 5000;

function useReadyServer() {
  const [ready, setReady] = useState(false);

  async function callServer() {
    try {
      const data = await fetch(`${serverUrl}/status/healthcheck`, );
      if (data.status !== 200) {
        throw new Error('Not correct status');
      }
      return true;
    } catch (e) {
      await new Promise((res) => setTimeout(res, RE_TIMEOUT));
      return callServer();
    }
  }

  useEffect(() => {
    if (!ready) {
      callServer()
        .then(() => setReady(true))
        .catch(console.error);
    }
  });

  return ready;
}

export default useReadyServer;
